#!/bin/python

import RPi.GPIO as GPIO
import time
import datetime

GPIO.setmode(GPIO.BCM)
GPIO.setup(23, GPIO.IN) #pin 7 = GPIO 4

ETATBOUT = 0
ETATBOUTPREC = 0
COMPTEUR = 0
t0 = time.mktime(time.gmtime())
hz = 10

fichier = open("/home/pi/speethondata.txt","a")

while 1:
    t1 = time.mktime(time.gmtime())
    ETATBOUT=GPIO.input(23)
    tc = t1-t0

    if (tc < hz) and (ETATBOUT != ETATBOUTPREC and ETATBOUT == 0):
        time.sleep(0.001)
        COMPTEUR=(COMPTEUR + 1)
        ETATBOUTPREC=ETATBOUT

    else:
        ETATBOUTPREC=ETATBOUT

    if (9.99 < tc) and (tc < 10.01):

#        if(COMPTEUR < 800):
#            varmoteur = varmoteur + 10
#        else:
#            varmoteur = varmoteur - 10

        t0 = t0 + hz
        print (time.asctime()) 
        print (COMPTEUR)
        fichier.write(time.asctime() + " ")
        fichier.write(str(COMPTEUR) + "\n")
        COMPTEUR = 0


